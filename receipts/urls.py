from django.urls import path
from receipts.views import (
    receipt_template,
    new_receipt,
    expense_category_list,
    account_list,
    create_category,
    create_account
    )


urlpatterns = [
    path("accounts/", account_list, name="account_list"),
    path("categories/", expense_category_list, name="category_list"),
    path("", receipt_template, name="home"),
    path("create/", new_receipt, name="create_receipt"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account")
]
